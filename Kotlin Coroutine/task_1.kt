import kotlinx.coroutines.*

suspend fun printlnworld() {
    delay(1000L)
    println("World!")
}

fun main() = runBlocking<Unit> {
    launch { printlnworld() }
    print("Hello, ")
    delay(2000L)
}
