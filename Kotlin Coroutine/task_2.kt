import kotlinx.coroutines.*

suspend fun firstint(): Int {
    delay(1000L)
    val x: Int = (0..10).random()
    return x
}

suspend fun secondint(): Int {
    delay(1000L)
    val y: Int = (0..10).random()
    return y
}

fun main() = runBlocking<Unit> {
    var res = firstint() + secondint()
    println(res)
    delay(2000L)

    val x = async {firstint()} //время выполнения нижнего куска кода будет меньше, т.к. асинхронно
    val y = async {secondint()} //выполняться, т.е. параллельно, в первом случае последовательно
    res = x.await() + y.await()
    print(res)
}
